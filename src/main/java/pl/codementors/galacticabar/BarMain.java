package pl.codementors.galacticabar;

import pl.codementors.galacticabar.model.Barman;
import pl.codementors.galacticabar.model.Client;

import java.util.Scanner;

/**
 * Main method of the application. Starts barman thread and five clients threads.
 *
 * @author Paweł Wieraszka
 */
public class BarMain {

    public static void main(String[] args) {
        System.out.println("Welcome to the bar.\nWrite 'quit' to exit\n");

        Barman barman1 = new Barman();
        Thread thread0 = new Thread(barman1);
        thread0.start();

        Client client1 = new Client("Waclaw", barman1);
        Client client2 = new Client("Eugeniusz", barman1);
        Client client3 = new Client("Wieslaw", barman1);
        Client client4 = new Client("Ryszard", barman1);
        Client client5 = new Client("Roman", barman1);
        Thread thread1 = new Thread(client1);
        Thread thread2 = new Thread(client2);
        Thread thread3 = new Thread(client3);
        Thread thread4 = new Thread(client4);
        Thread thread5 = new Thread(client5);
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        Scanner scanner = new Scanner(System.in);
        scanner.next();
        System.out.println("quit");
        barman1.stop();
        client1.stop();
        client2.stop();
        client3.stop();
        client4.stop();
        client5.stop();


        thread0.interrupt();
        thread1.interrupt();
        thread2.interrupt();
        thread3.interrupt();
        thread4.interrupt();
        thread5.interrupt();
    }
}
