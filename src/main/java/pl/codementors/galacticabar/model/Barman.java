package pl.codementors.galacticabar.model;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for Barman, that implements interface Runnable.
 *
 * @author Paweł Wieraszka
 */
public class Barman implements Runnable {

    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    /**
     * Name of a drink prepared by a barman.
     */
    private String drink = null;

    private boolean running = true;

    /**
     * Stops barman thread (stops making drinks by barman).
     */
    public void stop() {
        running = false;
    }

    /**
     * Barman puts drink on the bar and lets know customers.
     */
    public synchronized void putDrink() {
        this.drink = "Caipiroska";
        notifyAll();
    }

    /**
     * Informs that drink is ready to taken. Waits if barman is busy with drink preparation.
     *
     * @return Name of taken drink.
     * @throws InterruptedException
     */
    public synchronized String takeDrink() throws InterruptedException {
        while (this.drink == null) {
            wait();
        }
        String takenDrink = this.drink;
        this.drink = null;
        return takenDrink;
    }

    /**
     * If there isn't any drink on the bar, barman prepares drink and put it on the bar.
     * Else barman waits 3 seconds and one more time checks if there is any drink on the bar.
     */
    @Override
    public void run() {
        while (running) {
            if (drink == null) {
                putDrink();
            }
            else {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }
            }
        }
    }
}
