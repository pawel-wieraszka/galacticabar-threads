package pl.codementors.galacticabar.model;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for client, that implements interface Runnable.
 *
 * @author Paweł Wieraszka
 */
public class Client implements Runnable {

    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    /**
     * Client name.
     */
    private String name;

    /**
     * Instance of Barman class.
     */
    private Barman barman;

    private boolean running = true;

    public Client(String name, Barman barman) {
        this.name = name;
        this.barman = barman;
    }

    /**
     * Stops client thread (stops drinking by the client).
     */
    public void stop() {
        running = false;
    }

    /**
     * If there is any drink ready to be taken, all clients try to take the drink, bu only one succeed.
     */
    @Override
    public void run() {
        while (running) {
            try {
                String drink = barman.takeDrink();
                System.out.println(name+" took "+drink);
                Thread.sleep(drink.length()*1000);
                //System.out.println(name+" drank "+drink);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }
}
